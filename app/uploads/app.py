from flask import Flask, request, session, escape, url_for, make_response, render_template, redirect

app = Flask(__name__)
app.config.from_object('configs')
app.config["ENV"] = "development"
app.config["DEBUG"] = True
app.config['UPLOAD_FOLDER'] = 'uploads'
app.config['ALLOWED_EXTENSIONS'] = set(['txt', 'pdf', 'png', 'jpg', 'py', 'jpeg', 'gif'])

# @app.route("/text1", methods=['POST'])
# def index():
#     return request.form.get('value', 'No input')

# @app.route('/text2' )
# def get_value():
#     return redirect(url_for('index'))

# @app.route('/test3/<int:a>/<int:b>')
# def test3(a, b):
#     return str(a+b)

# @ app.route("/test4", methods=['GET'])
# def index1():
#     return request.args

# @app.route('/JSON-value', methods=['POST'])
# def JSON_value():
#     if request.is_json:
#         data = request.get_json()
#         result = data.get('input_value', None)
#     else:
#         result = 'Not JSON Data'
#     return result

from werkzeug.utils import secure_filename    
import os

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in app.config['ALLOWED_EXTENSIONS']

@app.route('/upload', methods=['POST'])
def upload():
    if request.method == 'POST':
        file = request.files['file']
        if file.filename == '':
            return 'No selected file'
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            return 'upload success'
        else:
            return 'file type not allowed'
        
# from markupsafe import escape           
# @app.route('/')
# def index():
#     return render_template('index.html')

# @app.route('/<username>')
# def user_home(username):
#     return render_template('index.html', username=escape(username))

# @app.errorhandler(404)
# def page_not_found(error):
#     return render_template('404.html', error=error)

# @app.route('/')                       //cookie
# def index():
#     return redirect(url_for('login'))

# @app.route('/login', methods=['GET', 'POST'])
# def login():
#     if request.method == "GET":
#         response = make_response(render_template('login.html'))
#     elif request.method == "POST":
#         account = request.values.get('username', None)
#         auth_result = 'success'
#         if auth_result == 'success':
#             response = make_response(redirect(url_for('home')))
#             response.set_cookie('account', account)
#         else:
#             response = make_response(redirect(url_for('index')))
#     return response

# @app.route('/home', methods=['GET'])
# def home():
#     if 'account' in request.cookies:
#         user = request.cookies.get('account')
#     else:
#         user = None
#     return render_template('home.html', username=user)

# # @app.route('/')                                            //session
# # def index():
# #     return redirect(url_for('login'))

# # @app.route('/login', methods=['GET', 'POST'])
# # def login():
# #     if request.method == "GET":
# #         response = make_response(render_template('res/login.html'))
# #     elif request.method == "POST":
# #         account = request.values.get('username', None)
# #         auth_result = 'success'
# #         if auth_result == 'success':
# #             response = make_response(redirect(url_for('home')))
# #             session['account'] = account
# #         else:
# #             response = make_response(redirect(url_for('index')))
# #     return response

# # @app.route('/home', methods=['GET'])
# # def home():
# #     print(session)
# #     if 'account' in session:
# #         user = session['account']
# #     else:
# #         user = None
# #     return render_template('res/home.html', username=user)

# # @app.route('/setting', methods=['GET'])
# # def settings():
# #     if 'account' in session:
# #         user = session['account']
# #     else:
# #         user = None
# #     return render_template('res/settings.html', username=user)

# @app.route('/')    
# def index():
#     return render_template('res/index.html')

# @app.route('/<username>')
# def user_home(username):
#     return render_template('res/home.html', username=escape(username))

# @app.route('/<int:time>')
# def user_list_home(time):
#     users = []
#     for i in range(time):
#         users.append("user" + str(i))

#     return render_template('res/home.html', users=users)

# @app.route("/test3", methods=['GET', 'POST'])
# def test3():
#     方法一
#     return request.form['input_value']

#     方法二
#     return request.form.get('input_value')
#     or3

#     print(request.args.get('a'))
#     a = int(request.args.get('a', 0))  
#     b = int(request.args.get('b', 0))  
#     result = a + b
#     return str(a+b)

# @app.route('/query', methods=['GET'])
# def query():
    # return request.args.values()

if __name__ == "__main__":
    app.run(debug=True)